const express = require('express')
const app = express();


//Import Routes
const cityWeather = require('./routes/city');
//ROUTES
app.use('/city',cityWeather );

app.get('/', (req, res) => {
    res.send('we are on home');
});

//HOW TO WE START LISTENING TO THE SERVER
app.listen(3000);
