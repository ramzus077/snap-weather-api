const express = require("express");
const router = express.Router();

//GET THE CITY WEATHER INFORMATIONS 
// TODO: RETURN RANDOM DATA
router.get("/:cityName", async (req, res) => {
  try {
    console.log(`Get ${req.params.cityName} weather`);
    const cityWeatherInfo = {
      id: 0,
      city: req.params.cityName,
      description: "sunny",
      degree: 30,
    };
    res.json(cityWeatherInfo);
  } catch (error) {
    res.json({ message: error });
    console.log("error: " + error);
  }
});

module.exports = router;